
from django.db import models

class liga(models.Model):
    codigo = models.CharField(primary_key=True, max_length=50)    
    equipo = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    estadio = models.CharField(max_length=50)
    numero_jugador = models.CharField(max_length=3)
    capacidad = models.IntegerField(default=5000)
    nombre_dueño = models.CharField(max_length=50)
    posicion_jugador = models.CharField(max_length=50)
    Nombre_Jugador= models.CharField(max_length=50)

    def __str__(self):
        return f"{self.equipo}, {self.ciudad}, {self.estadio}, {self.numero_jugador}, {self.capacidad}, {self.nombre_dueño}, {self.posicion_jugador}, {self.Nombre_Jugador}"
