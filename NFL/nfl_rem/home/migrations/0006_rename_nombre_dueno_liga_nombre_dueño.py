# Generated by Django 3.2.12 on 2023-12-06 05:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_rename_nombre_jugador_liga_nombre_jugador'),
    ]

    operations = [
        migrations.RenameField(
            model_name='liga',
            old_name='nombre_dueno',
            new_name='nombre_dueño',
        ),
    ]
