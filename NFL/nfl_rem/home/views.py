# views.py

from django.shortcuts import render, redirect
from .models import liga

def index(request):
    nfl = liga.objects.all()
    return render(request, "home/admin.html", {"liga": nfl})

def registrarDatos(request):
    codigo = request.POST["txtCodigo"]
    equipo = request.POST["txtEquipo"]
    ciudad = request.POST["txtCiudad"]
    estadio = request.POST["txtEstadio"]
    numero_jugador = request.POST["txtNumJugador"]
    capacidad = request.POST["txtCapacidad"]  
    nombre_dueño = request.POST["txtNomDueño"]
    posicion_jugador = request.POST["txtPosJugador"]
    Nombre_Jugador = request.POST["txtNomJugador"]
    
    nfl = liga.objects.create(
        codigo=codigo,
        equipo=equipo,
        ciudad=ciudad,
        estadio=estadio,
        numero_jugador=numero_jugador,
        capacidad=capacidad,
        nombre_dueño=nombre_dueño,
        posicion_jugador=posicion_jugador,
        Nombre_Jugador=Nombre_Jugador
    )
    return redirect("/")

def edicionDatos(request, codigo):
    nfl = liga.objects.get(codigo=codigo)
    return render(request, "edicionDatos.html", {"liga": nfl})

def editarDatos(request):
    codigo = request.POST["txtCodigo"]
    equipo = request.POST["txtEquipo"]
    ciudad = request.POST["txtCiudad"]
    estadio = request.POST["txtEstadio"]
    numero_jugador = request.POST["txtNumJugador"]
    capacidad = request.POST["txtCapacidad"]  
    nombre_dueño = request.POST["txtNomDueño"]
    posicion_jugador = request.POST["txtPosJugador"]
    Nombre_Jugador = request.POST["txtNomJugador"]

    nfl = liga.objects.get(codigo=codigo)
    nfl.equipo = equipo
    nfl.ciudad = ciudad
    nfl.estadio = estadio
    nfl.numero_jugador = numero_jugador
    nfl.capacidad = capacidad
    nfl.nombre_dueño = nombre_dueño
    nfl.posicion_jugador = posicion_jugador
    nfl.Nombre_Jugador= Nombre_Jugador
    nfl.save()

    return redirect("/")

def eliminarDatos(request, codigo):
    nfl = liga.objects.get(codigo=codigo)
    nfl.delete()
    
    return redirect("/")



    